import styles from '../styles/home.module.scss'
import { SEO } from '../components/SEO'

function Home() {
	return (
		<>
			<SEO title="Dev News!" excludeTitleSuffix />
			<main className={styles.content}>
				<section className={styles.section}>
					<span>Hello Developers!</span>
					<h1>
						Welcome to <span>Dev</span> News!
					</h1>
					<p>
						Dev News é um projeto de estudo para aprimorar <br />
						meus conhecimentos em <span> ReactJS, NextJS e TypeScript. </span>
					</p>
				</section>
				<picture>
					<img src="/home.svg" alt="Home image" />
				</picture>
			</main>
		</>
	)
}

export default Home
