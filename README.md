# <div align="center"> Web Dev News </div>

</br>

<div align="center">
<p> Web application developed to be used as a blog to help people who are eating their careers </p>
</div>

</br>

![image 1](/.github/image.png "Image 1")

</br>

## Technologies used in the project

- Typescript
- NextJs
- Sass
- Prismic

## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/web-dev-news.git
```

</br>

- Start the Application in Development

```shell
yarn install
yarn dev
```

</br>

- To stop the Application click CTRL+C in your terminal
