import { GetStaticPaths, GetStaticProps } from 'next'
import { useRouter } from 'next/router'
import { RichText } from 'prismic-dom'
import { SEO } from '../../components/SEO'
import { getPrismicClient } from '../../services/prismic'
import styles from './post.module.scss'

interface PostProps {
	post: {
		slug: string
		title: string
		content: string
		updatedAt: string
	}
}

interface PrismicDocumentTitle {
	type: string
	text: string
}

interface PrismicDocumentContent {
	type: string
	text: string
}

interface PrismicDocument {
	uid: string
	data: {
		title: PrismicDocumentTitle[]
		content: PrismicDocumentContent[]
	}
}

function Post({ post }: PostProps) {
	const router = useRouter()

	if (router.isFallback) {
		return <p>Loading...</p>
	}

	return (
		<>
			<SEO title="Post" />
			<main className={styles.container}>
				<article className={styles.post}>
					<h1>{post.title}</h1>
					<div className={styles.content} dangerouslySetInnerHTML={{ __html: post.content }} />
				</article>
			</main>
		</>
	)
}

export default Post

export const getStaticPaths: GetStaticPaths = async () => {
	return {
		paths: [],
		fallback: true
	}
}

export const getStaticProps: GetStaticProps = async (context) => {
	const { id } = context.params as { id: string }
	const prismic = getPrismicClient()
	const response = await prismic.getByUID('post', String(id), {})
	const document = response as PrismicDocument
	const post = {
		id,
		title: RichText.asText(document.data.title),
		content: RichText.asHtml(document.data.content)
	}
	return {
		props: {
			post
		}
	}
}
